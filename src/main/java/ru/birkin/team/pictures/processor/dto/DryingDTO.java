package ru.birkin.team.pictures.processor.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 09.03.2021
 * Time: 21:28
 */
@Data
public class DryingDTO {

    private TumbleDryingType tumbleDryingType;

    private NaturalDryingType naturalDryingType;

    public enum NaturalDryingType {
        LINE_DRY,
        DRY_FLAT,
        DRIP_DRY,
        DRY_IN_SHADE,
        LINE_DRY_IN_SHADE,
        DRY_FLAT_IN_SHADE,
        DRIP_DRY_IN_SHADE
    }

    public enum TumbleDryingType {
        LOW_TEMPERATURE,
        NORMAL,
        DO_NOT_TUMBLE_DRY
    }
}
