package ru.birkin.team.pictures.processor.web;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.birkin.team.pictures.processor.api.LaundryTagAPI;
import ru.birkin.team.pictures.processor.dto.DecodedLaundrySymbolDTO;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 09.03.2021
 * Time: 22:24
 */
@RestController
@RequestMapping("/api")
public class LaundryTagController {

    private final LaundryTagAPI laundryTagAPI;

    public LaundryTagController(final LaundryTagAPI laundryTagAPI) {this.laundryTagAPI = laundryTagAPI;}

    @ApiOperation(value = "Decode laundry tag by photo")
    @PostMapping(value = "/decodeLaundryTag", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<DecodedLaundrySymbolDTO> decodeLaundryTag(
            final @ApiParam(value = "Photo to upload", required = true) @RequestParam("file") MultipartFile file)
            throws IOException {
        return ResponseEntity.ok(laundryTagAPI.decodeLaundryTag(file.getBytes()));
    }
}
