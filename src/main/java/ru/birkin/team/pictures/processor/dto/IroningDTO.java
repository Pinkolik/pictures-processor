package ru.birkin.team.pictures.processor.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 09.03.2021
 * Time: 21:39
 */
@Data
public class IroningDTO {

    private IroningType type;

    public enum IroningType {
        LOW_TEMPERATURE,
        MEDIUM_TEMPERATURE,
        HIGH_TEMPERATURE,
        DO_NOT_IRON
    }
}
