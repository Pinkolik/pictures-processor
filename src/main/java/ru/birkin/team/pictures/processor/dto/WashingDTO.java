package ru.birkin.team.pictures.processor.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 09.03.2021
 * Time: 21:15
 */
@Data
public class WashingDTO {

    private Integer maxTemperature;

    private WashType type;

    public enum WashType {
        NORMAL_WASH,
        HAND_WASH,
        DO_NOT_WASH
    }
}
