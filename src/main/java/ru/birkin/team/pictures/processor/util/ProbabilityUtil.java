package ru.birkin.team.pictures.processor.util;

import org.springframework.stereotype.Component;
import ru.birkin.team.pictures.processor.dto.DryingDTO;
import ru.birkin.team.pictures.processor.dto.IroningDTO;
import ru.birkin.team.pictures.processor.dto.ProfessionalCleaningDTO;

import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 14.05.2021
 * Time: 15:22
 */
@Component
public class ProbabilityUtil {

    private final Random random = new Random();

    public Integer getMaxTemperature() {
        //10% - 40, 90% - 30
        int chance = random.nextInt(100);
        if (chance < 10) {
            return 40;
        }
        else {
            return 30;
        }
    }

    public DryingDTO.TumbleDryingType getTumbleDryingType() {
        //NORMAL 50%, DO_NOT_TUMBLE_DRY 50%
        int chance = random.nextInt(100);
        if (chance < 50) {
            return DryingDTO.TumbleDryingType.NORMAL;
        }
        else {
            return DryingDTO.TumbleDryingType.DO_NOT_TUMBLE_DRY;
        }
    }

    public IroningDTO.IroningType getIroningType() {
        //LOW_TEMPERATURE 80%, DO_NOT_IRON 20%
        int chance = random.nextInt(100);
        if (chance < 20) {
            return IroningDTO.IroningType.DO_NOT_IRON;
        }
        else {
            return IroningDTO.IroningType.LOW_TEMPERATURE;
        }
    }

    public ProfessionalCleaningDTO getProfessionalCleaning() {
        //null - 70%, PCE - 30%
        int chance = random.nextInt(100);
        if (chance < 30) {
            ProfessionalCleaningDTO professionalCleaning = new ProfessionalCleaningDTO();
            professionalCleaning.setChemicalCleaningType(ProfessionalCleaningDTO.ChemicalCleaningType.PCE);
            return professionalCleaning;
        }
        else {
            return null;
        }
    }

}
