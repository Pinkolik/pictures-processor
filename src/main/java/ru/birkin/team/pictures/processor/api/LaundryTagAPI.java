package ru.birkin.team.pictures.processor.api;

import ru.birkin.team.pictures.processor.dto.DecodedLaundrySymbolDTO;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 09.03.2021
 * Time: 22:25
 */
public interface LaundryTagAPI {

    DecodedLaundrySymbolDTO decodeLaundryTag(byte[] picture);
}
