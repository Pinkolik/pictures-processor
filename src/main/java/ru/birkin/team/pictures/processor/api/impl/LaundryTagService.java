package ru.birkin.team.pictures.processor.api.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import ru.birkin.team.pictures.processor.api.LaundryTagAPI;
import ru.birkin.team.pictures.processor.dto.*;
import ru.birkin.team.pictures.processor.dto.BleachingDTO.BleachingType;
import ru.birkin.team.pictures.processor.dto.WashingDTO.WashType;
import ru.birkin.team.pictures.processor.util.ProbabilityUtil;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 09.03.2021
 * Time: 22:26
 */
@Service
@Slf4j
public class LaundryTagService implements LaundryTagAPI {

    private final ProbabilityUtil probabilityUtil;

    public LaundryTagService(final ProbabilityUtil probabilityUtil) {this.probabilityUtil = probabilityUtil;}

    @Override
    public DecodedLaundrySymbolDTO decodeLaundryTag(final byte[] picture) {
        File file = new File("python" + File.separator + UUID.randomUUID().toString() + ".png");
        try {
            FileUtils.writeByteArrayToFile(file, picture);
            log.info("Saved file {}", file.getAbsolutePath());
        }
        catch (IOException e) {
            log.error("Error on saving file {}", file.getAbsolutePath(), e);
            return null;
        }
        return getRandomDecodedLaundrySymbol();
    }

    private DecodedLaundrySymbolDTO getRandomDecodedLaundrySymbol() {
        DecodedLaundrySymbolDTO decodedLaundrySymbol = new DecodedLaundrySymbolDTO();

        WashingDTO washing = new WashingDTO();
        washing.setMaxTemperature(probabilityUtil.getMaxTemperature());
        washing.setType(WashType.NORMAL_WASH);
        decodedLaundrySymbol.setWashing(washing);

        BleachingDTO bleaching = new BleachingDTO();
        bleaching.setType(BleachingType.DO_NOT_BLEACH);
        decodedLaundrySymbol.setBleaching(bleaching);

        DryingDTO drying = new DryingDTO();
        drying.setTumbleDryingType(probabilityUtil.getTumbleDryingType());
        drying.setNaturalDryingType(null);
        decodedLaundrySymbol.setDrying(drying);

        IroningDTO ironing = new IroningDTO();
        ironing.setType(probabilityUtil.getIroningType());
        decodedLaundrySymbol.setIroning(ironing);

        ProfessionalCleaningDTO professionalCleaning = probabilityUtil.getProfessionalCleaning();
        decodedLaundrySymbol.setProfessionalCleaning(professionalCleaning);
        return decodedLaundrySymbol;
    }

}
