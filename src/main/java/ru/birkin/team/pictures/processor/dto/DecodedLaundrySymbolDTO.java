package ru.birkin.team.pictures.processor.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 09.03.2021
 * Time: 21:10
 */
@Data
public class DecodedLaundrySymbolDTO {

    private WashingDTO washing;

    private BleachingDTO bleaching;

    private DryingDTO drying;

    private IroningDTO ironing;

    private ProfessionalCleaningDTO professionalCleaning;
}
