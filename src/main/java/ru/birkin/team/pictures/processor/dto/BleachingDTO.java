package ru.birkin.team.pictures.processor.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 09.03.2021
 * Time: 21:23
 */
@Data
public class BleachingDTO {

    private BleachingType type;

    public enum BleachingType {
        ANY_BLEACH_ALLOWED,
        ONLY_OXYGEN_BLEACH_ALLOWED,
        DO_NOT_BLEACH
    }
}
