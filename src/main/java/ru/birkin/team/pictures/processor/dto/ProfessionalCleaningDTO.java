package ru.birkin.team.pictures.processor.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 09.03.2021
 * Time: 21:42
 */
@Data
public class ProfessionalCleaningDTO {

    private ChemicalCleaningType chemicalCleaningType;

    private WetCleaningType wetCleaningType;

    public enum ChemicalCleaningType {
        HCS,
        PCE,
        DO_NOT_DRY_CLEAN
    }

    public enum WetCleaningType {
        NORMAL,
        DO_NOT_WET_CLEAN
    }
}
