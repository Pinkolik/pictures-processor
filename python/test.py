import sys


def get_decoded_tag(path_to_image):
    print("""{
  "washing": {
    "maxTemperature": null,
    "type": "NORMAL_WASH",
    "agitationLevel": 0
  },
  "bleaching": {
    "type": "ANY_BLEACH_ALLOWED"
  },
  "drying": {
    "tumbleDryingType": null,
    "naturalDryingType": "DRIP_DRY"
  },
  "ironing": {
    "type": "LOW_TEMPERATURE"
  },
  "professionalCleaning": null
}""")


if __name__ == "__main__":
    get_decoded_tag(sys.argv[1])
